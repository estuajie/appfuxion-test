import React from 'react'
import { Autocomplete, TextField, Typography } from '@mui/material';

import { useDispatch } from "react-redux";
import usePlacesAutocomplete from "use-places-autocomplete";
import { handleGeoCode } from '../redux/action/index'

import Card from '@mui/material/Card';


export default function SearchBox() {
    const dispatch = useDispatch()
    const {
        ready,
        value,
        setValue,
        suggestions: { data },
        clearSuggestions,
    } = usePlacesAutocomplete();

    const handleSelect = async (event) => {
        setValue(event.target.outerText, false);
        clearSuggestions();
        dispatch(handleGeoCode(event.target.outerText))
    };
    const handleInput = (event) => {
        setValue(event.target.value);
    };

    return (
        <Card sx={{ p: 3 }}>
            <Typography variant='h5' sx={{ fontWeight: 'bold', mb: 2, mt: -1}}>Please input your city</Typography>
            <Autocomplete
                disableClearable
                onChange={handleSelect}
                value={data.find(x => x.description === value)}
                disablePortal={!ready}
                options={data}
                filterOptions={x => x}
                getOptionLabel={option =>
                    typeof option === 'string' ? option : option.description
                }
                sx={{ width: "100%" }}
                renderInput={(params) => <TextField {...params} onChange={event => handleInput(event)} label="Type something" />}
            />
        </Card>
    );
}
