import React from 'react'
import { Box, Typography } from "@mui/material";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: '#fff',
    borderColor: 'grey.500',
    border: '1px solid #fff !important',
    p: 4,
    textAlign: 'center'
  };

export default function Loader() {
    return (
        <div>
            <Box sx={style}>
                <Typography variant="p">
                    Please wait...
                </Typography>
            </Box>
        </div>
    )
}
