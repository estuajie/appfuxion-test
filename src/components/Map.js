import React from 'react'
import { GoogleMap, MarkerF } from "@react-google-maps/api";
import { useSelector } from "react-redux";
import { Box } from '@mui/material';
import SearchBox from './SearchBox';

export default function Map() {
    const stateGlobal = useSelector(state => state.cordinateReducer)

    return (
        <>
            <Box sx={{ mt: 10, width: { xs: '94%', sm: '40%' } }} className="places-container">
                <SearchBox />
            </Box>

            <GoogleMap
                zoom={11}
                center={stateGlobal.selectedCordinate === null ? stateGlobal.defaultCordinate : stateGlobal.selectedCordinate}
                mapContainerClassName="map-container"
            >
                {stateGlobal.defaultCordinate && <MarkerF position={stateGlobal.selectedCordinate === null ? stateGlobal.defaultCordinate : stateGlobal.selectedCordinate} />}
            </GoogleMap>
        </>
    );
}
