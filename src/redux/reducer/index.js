import { combineReducers } from "redux";
import cordinateReducer from "./cordinateReducer";

const reducer = combineReducers({ cordinateReducer })

export default reducer;