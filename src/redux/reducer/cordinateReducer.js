
const initialState = {
    defaultCordinate: { lat: -6.2087634, lng: 106.845599 },
    selectedCordinate: null
}

const cordinateReducer = (state = initialState, action) => {
    if (action.type === 'UPDATE_SELECTED') {
        return {
            ...state,
            selectedCordinate: action.payload
        }
    }
    return state;
}

export default cordinateReducer;