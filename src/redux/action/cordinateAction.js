import { getGeocode, getLatLng } from "use-places-autocomplete";

export const handleGeoCode = (payload) => {
    return async function (dispatch) {
        const results = await getGeocode({ address: payload });
        const { lat, lng } = await getLatLng(results[0]);
        dispatch({ type: 'UPDATE_SELECTED', payload: { lat, lng } })
    }
}