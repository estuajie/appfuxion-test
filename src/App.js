import { useLoadScript } from "@react-google-maps/api";

import Map from "./components/Map"
import Loader from "./components/Loader";

export default function App() {
  const { isLoaded } = useLoadScript({
    googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAP_KEY,
    libraries: ["places"],
  });

  return (
    <>
      {!isLoaded ? (
        <Loader />
      ) : (
        <Map />
      )}
    </>
  )
}